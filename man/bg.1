BG(1POSIX)     POSIX Programmer's Manual     BG(1POSIX)

PROLOG
       This  manual  page is part of the POSIX Program-
       mer's Manual.  The Linux implementation of  this
       interface  may differ (consult the corresponding
       Linux manual page for details  of  Linux  behav-
       ior), or the interface may not be implemented on
       Linux.

NAME
       bg -- run jobs in the background

SYNOPSIS
       bg [job_id...]

DESCRIPTION
       If job control is enabled (see  the  description
       of  set  -m),  the  bg utility shall resume sus-
       pended jobs from the  current  environment  (see
       Section  2.12,  Shell  Execution Environment) by
       running them as  background  jobs.  If  the  job
       specified  by  job_id is already a running back-
       ground job, the bg utility shall have no  effect
       and shall exit successfully.

       Using  bg  to  place  a  job into the background
       shall cause its process ID to become ``known  in
       the current shell execution environment'', as if
       it had been started as an asynchronous list; see
       Section 2.9.3.1, Examples.

OPTIONS
       None.

OPERANDS
       The following operand shall be supported:

       job_id    Specify  the  job  to  be resumed as a
                 background job. If no  job_id  operand
                 is  given, the most recently suspended
                 job  shall  be  used.  The  format  of
                 job_id  is described in the Base Defi-
                 nitions volume of  POSIX.1-2008,  Sec-
                 tion 3.204, Job Control Job ID.

STDIN
       Not used.

INPUT FILES
       None.

ENVIRONMENT VARIABLES
       The following environment variables shall affect
       the execution of bg:

       LANG      Provide a default value for the inter-
                 nationalization   variables  that  are
                 unset or null. (See the  Base  Defini-
                 tions  volume of POSIX.1-2008, Section
                 8.2,  Internationalization   Variables
                 for  the  precedence of international-
                 ization variables  used  to  determine
                 the values of locale categories.)

       LC_ALL    If  set  to  a non-empty string value,
                 override the values of all  the  other
                 internationalization variables.

       LC_CTYPE  Determine the locale for the interpre-
                 tation of sequences of bytes  of  text
                 data  as characters (for example, sin-
                 gle-byte  as  opposed  to   multi-byte
                 characters in arguments).

       LC_MESSAGES
                 Determine  the  locale  that should be
                 used to affect the format and contents
                 of   diagnostic  messages  written  to
                 standard error.

       NLSPATH   Determine the location of message cat-
                 alogs  for  the  processing of LC_MES-
                 SAGES.

ASYNCHRONOUS EVENTS
       Default.

STDOUT
       The output of bg shall consist of a line in  the
       format:

           "[%d] %s\n", <job-number>, <command>

       where the fields are as follows:

       <job-number>
                 A  number that can be used to identify
                 the job to  the  wait,  fg,  and  kill
                 utilities.  Using these utilities, the
                 job can be identified by prefixing the
                 job number with '%'.

       <command> The  associated command that was given
                 to the shell.

STDERR
       The standard error shall be used only for  diag-
       nostic messages.

OUTPUT FILES
       None.

EXTENDED DESCRIPTION
       None.

EXIT STATUS
       The following exit values shall be returned:

        0    Successful completion.

       >0    An error occurred.

CONSEQUENCES OF ERRORS
       If job control is disabled, the bg utility shall
       exit with an error and no job shall be placed in
       the background.

       The following sections are informative.

APPLICATION USAGE
       A  job is generally suspended by typing the SUSP
       character (<control>-Z on most systems); see the
       Base Definitions volume of POSIX.1-2008, Chapter
       11, General Terminal Interface.  At that  point,
       bg  can put the job into the background. This is
       most effective when the job is expecting no ter-
       minal  input  and its output has been redirected
       to non-terminal files. A background job  can  be
       forced  to  stop  when it has terminal output by
       issuing the command:

           stty tostop

       A background job can be stopped  with  the  com-
       mand:

           kill -s stop job ID

       The bg utility does not work as expected when it
       is operating in its own utility execution  envi-
       ronment  because  that  environment  has no sus-
       pended jobs. In the following examples:

           ... | xargs bg
           (bg)

       each bg operates in a different environment  and
       does  not share its parent shell's understanding
       of jobs. For this reason, bg is generally imple-
       mented as a shell regular built-in.

EXAMPLES
       None.

RATIONALE
       The  extensions  to  the shell specified in this
       volume of POSIX.1-2008 have mostly been based on
       features provided by the KornShell. The job con-
       trol features provided by bg, fg, and  jobs  are
       also based on the KornShell. The standard devel-
       opers examined  the  characteristics  of  the  C
       shell versions of these utilities and found that
       differences exist. Despite widespread use of the
       C  shell,  the  KornShell versions were selected
       for this volume of POSIX.1-2008  to  maintain  a
       degree  of uniformity with the rest of the Korn-
       Shell features selected (such as the very  popu-
       lar command line editing features).

       The bg utility is expected to wrap its output if
       the output exceeds the number  of  display  col-
       umns.

FUTURE DIRECTIONS
       None.

SEE ALSO
       Section 2.9.3.1, Examples, fg, kill, jobs, wait

       The  Base  Definitions  volume  of POSIX.1-2008,
       Section 3.204, Job Control Job  ID,  Chapter  8,
       Environment  Variables, Chapter 11, General Ter-
       minal Interface

COPYRIGHT
       Portions of this text are reprinted  and  repro-
       duced  in  electronic form from IEEE Std 1003.1,
       2013 Edition, Standard for Information  Technol-
       ogy   --  Portable  Operating  System  Interface
       (POSIX),  The  Open  Group  Base  Specifications
       Issue  7, Copyright (C) 2013 by the Institute of
       Electrical and Electronics  Engineers,  Inc  and
       The  Open Group.  (This is POSIX.1-2008 with the
       2013 Technical Corrigendum 1  applied.)  In  the
       event  of  any  discrepancy between this version
       and the original IEEE and The Open  Group  Stan-
       dard, the original IEEE and The Open Group Stan-
       dard is the referee document. The original Stan-
       dard     can     be     obtained    online    at
       http://www.unix.org/online.html .

       Any  typographical  or  formatting  errors  that
       appear in this page are most likely to have been
       introduced during the conversion of  the  source
       files to man page format. To report such errors,
       see https://www.kernel.org/doc/man-pages/report-
       ing_bugs.html .

IEEE/The Open Group       2013               BG(1POSIX)
