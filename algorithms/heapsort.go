///usr/bin/env go run $0 shared.go $@; exit
package main

import "fmt"

func main() {
	a := Generate()
	fmt.Println(a)
	Heapsort(a)
	fmt.Println(a)
}

// Heapsort sorts a slice like a heap.
func Heapsort(a []int) {
	// heapify
	for i := len(a)/2 - 1; i >= 0; i-- {
		down(a, len(a), i)
	}
	for size, i := len(a)-1, len(a)-1; i > 0; size, i = size-1, i-1 {
		a[0], a[i] = a[i], a[0]
		down(a, size, 0)
	}
}

func down(a []int, size int, i int) {
	j := i
	l := 2*i + 1
	r := l + 1

	if l < size && a[l] > a[j] {
		j = l
	}
	if r < size && a[r] > a[j] {
		j = r
	}

	if j != i {
		a[i], a[j] = a[j], a[i]
		down(a, size, j)
	}
}
