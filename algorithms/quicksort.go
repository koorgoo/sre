///usr/bin/env go run $0 shared.go $@; exit
package main

import "fmt"

var (
	noRecursion = -1
	recursions  = noRecursion
)

func main() {
	a := Generate()

	fmt.Println(a)
	QuickSort(a)
	fmt.Printf("%v - %d recursions\n", a, recursions)

	recursions = noRecursion
	QuickSort(a)
	fmt.Printf("%v - %d recursions when sorted\n", a, recursions)
}

// QuickSort sorts a in place.
func QuickSort(a []int) {
	if len(a) <= 1 {
		return
	}
	recursions++
	q := partition(a)
	QuickSort(a[:q])
	QuickSort(a[q+1:])
}

func partition(a []int) int {
	last := len(a) - 1
	i := -1
	for j := 0; j < last; j++ {
		if a[j] <= a[last] {
			i++
			a[i], a[j] = a[j], a[i]
		}
	}
	a[i+1], a[last] = a[last], a[i+1]
	return i + 1
}
