package main

import (
	"flag"
	"math/rand"
	"time"
)

func init() {
	flag.Parse()
	rand.Seed(time.Now().Unix())
}

var (
	size   = flag.Int("n", 10, "amount of numbers")
	bound  = flag.Int("u", 100, "upper bound of numbers")
	signed = flag.Bool("s", false, "use signed numbers")
)

// GenerateOpts configures Generate result.
type GenerateOpts struct {
	Size   int
	Bound  int
	Signed bool
}

func (o *GenerateOpts) set() {
	o.Size = *size
	o.Bound = *bound
	o.Signed = *signed
}

func (o *GenerateOpts) sign() int {
	if o.Signed {
		return randomSign()
	}
	return 1
}

// Generate returns a slice of random integers.
func Generate() []int {
	var opts GenerateOpts
	opts.set()

	a := make([]int, opts.Size)
	for i := range a {
		a[i] = opts.sign() * rand.Intn(opts.Bound)
	}
	return a
}

func randomSign() int {
	n := rand.Uint32()
	if (n >> 31) == 0 {
		return -1
	}
	return 1
}
