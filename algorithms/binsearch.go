///usr/bin/env go run $0 shared.go $@; exit
package main

import (
	"fmt"
	"math/rand"
	"sort"
)

var iterations = 0

func main() {
	a := Generate()
	sort.Ints(a)

	n1 := a[rand.Intn(len(a))]
	n2 := a[len(a)-1] + 1000 // will not be found

	fmt.Println(a)
	for _, n := range []int{n1, n2} {
		i := BinarySearch(a, n)
		fmt.Printf("index of %d is %d\t(%d iterations)\n", n, i, iterations)
	}
}

// BinarySearch returns an index of n in a sorted list a.
// It returns -1 when n is not found in a.
func BinarySearch(a []int, n int) int {
	iterations = 0

	start := 0
	end := len(a)

	for {
		if start == end {
			return -1
		}

		iterations++

		middle := (start + end) / 2
		if a[middle] == n {
			return middle
		}

		if a[middle] > n {
			end = middle
		} else {
			start = middle + 1
		}
	}
}
