#!/usr/bin/env python
import argparse
import os
import os.path
import subprocess as sp
from collections import namedtuple
from contextlib import contextmanager

# Working Copy
IPHONE_PLUS_COLS = 57


Manual = namedtuple('Manual', ('name', 'section', 'path', 'size'))


def list_manuals(man_dir='man'):
    for filename in os.listdir(man_dir):
        path = os.path.join(man_dir, filename)
        name, ext = os.path.splitext(filename)
        section = ext[1:] or None
        size = os.path.getsize(path)
        yield Manual(name=name, section=section, path=path, size=size)


@contextmanager
def terminal(columns):
    prev = sp.check_output(['tput', 'cols'])
    sp.check_output(['stty', 'cols', str(columns)])
    yield
    sp.check_output(['stty', 'cols', prev.strip()])


def read_manual(manual):
    p = sp.Popen(man_command(manual), stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = p.communicate()
    if p.returncode:
        print('{}: {}'.format(manual.name, err))
        return None
    return out


def man_command(manual):
    result = ['man', '-P', 'cat']
    if manual.section is not None:
        result.append(str(manual.section))
    result.append(manual.name)
    return result


def write_manual(manual, text):
    with open(manual.path, 'w') as f:
        if text is not None:
            f.write(text)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--all', dest='all', action='store_true',
                        help='all manuals (empty manuals by default)')
    args = parser.parse_args()

    with terminal(IPHONE_PLUS_COLS):
        for manual in list_manuals():
            if manual.size and not args.all:
                continue

            text = read_manual(manual)
            write_manual(manual, text)
